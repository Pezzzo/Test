'use strict';

$(document).ready(() => {
  $('.modal__link--reg').click((evt) => {
    evt.preventDefault();
    console.log('click');
    if ($('.modal-reg').is(':hidden')) {
      $('.modal-reg').fadeIn(300);
      $('.page').addClass('page--modal-reg');

      let x = window.scrollX;
      let y = window.scrollY;
      window.onscroll = () => {
        window.scrollTo(x, y);
      };
    }
  });
});

const Reg = () => {
  return React.createElement('div', { className: 'modal-reg modal' },
    React.createElement('button', { className: 'modal__close-reg-button' },
      React.createElement('img', {
        className: 'modal__close-button-img',
        src: 'img/ui-icons/modal-close.svg',
        alt: 'кнопка закрытия'
      })),
    React.createElement('div', { className: 'modal__wrapper' },
      React.createElement('h1', { className: 'modal__title uppercase-mod' }, 'Регистрация'),
      React.createElement('form', { className: 'form form--modal form--reg', action: '#' },
        React.createElement('label', { className: 'form__input-wrapper form__input-wrapper--modal' },
          React.createElement('span', { className: 'form__text form__text--modal' }, 'E-mail'),
          React.createElement('input', {
            className: 'form__input form__input--modal form__input--reg',
            type: 'email',
            name: 'email',
            required: true
          },
          )),
        React.createElement('label', { className: 'form__input-wrapper form__input-wrapper--modal' },
          React.createElement('span', { className: 'form__text form__text--modal' }, 'Логин'),
          React.createElement('input', {
            className: 'form__input form__input--modal form__input--reg',
            type: 'text',
            name: 'login',
            required: true
          },
          )),
        React.createElement('label', { className: 'form__input-wrapper form__input-wrapper--modal' },
          React.createElement('span', { className: 'form__text form__text--modal' }, 'Пароль'),
          React.createElement('input', {
            className: 'form__input form__input--modal form__input--reg',
            type: 'password',
            name: 'password',
            required: true
          },
          )),
        React.createElement('label', { className: 'form__input-wrapper form__input-wrapper--modal' },
          React.createElement('span', { className: 'form__text form__text--modal' }, 'Повторите пароль'),
          React.createElement('input', {
            className: 'form__input form__input--modal form__input--reg',
            type: 'password',
            name: 'password',
            required: true
          },
          )),
        React.createElement('label', { className: 'form__input-checkbox-wrapper' },
          React.createElement('input', {
            className: 'form__input-checkbox visually-hidden',
            type: 'checkbox',
            name: 'checkbox'
          }),
          React.createElement('span', { className: 'form__text' }, 'Согласен с политикой конфидициальности'),
        ),
        React.createElement('button', {
          className: 'form__modal-submit form__modal-submit--reg',
          type: 'submit',
          disabled: true
        }, 'Регистрация'),
      ),
    )
  )
};

const reg = React.createElement;
const regRoot = ReactDOM.createRoot(document.querySelector('.registration-container'));
regRoot.render(reg(Reg));

$(document).ready(() => {
  $('.modal__close-reg-button').click((evt) => {
    evt.preventDefault();
    $('.modal-reg').fadeOut(300);
    $('.page').removeClass('page--modal-reg')
  });
});

$(document).ready(() => {
  $('.form').on('change', () => {
    if ($('.form__input-checkbox').is(':checked')) $('.form__modal-submit--reg').attr('disabled', false);
    else $('.form__modal-submit--reg').attr('disabled', true);
  });
  $('.form').submit((evt) => {
    evt.preventDefault();
  });
});



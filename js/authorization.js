'use strict';

$(document).ready(() => {
  $('.auth__link').click((evt) => {
    evt.preventDefault();
    console.log('click');
    if ($('.modal-auth').is(':hidden')) {
      $('.modal-auth').fadeIn(300);
      $('.page').addClass('page--modal-auth');

      let x = window.scrollX;
      let y = window.scrollY;
      window.onscroll = () => {
        window.scrollTo(x, y);
      };
    }
  });
});

const Auth = () => {
  return React.createElement('div', { className: 'modal-auth modal' },
    React.createElement('button', { className: 'modal__close-auth-button' },
      React.createElement('img', {
        className: 'modal__close-button-img',
        src: 'img/ui-icons/modal-close.svg',
        alt: 'кнопка закрытия'
      })),
    React.createElement('div', { className: 'modal__wrapper' },
      React.createElement('h1', { className: 'modal__title uppercase-mod' }, 'Авторизация'),
      React.createElement('form', { className: 'form form--auth', action: '#' },
        React.createElement('label', { className: 'form__input-wrapper form__input-wrapper--modal' },
          React.createElement('span', { className: 'form__text form__text--modal' }, 'Логин / E-mail'),
          React.createElement('input', {
            className: 'form__input form__input--modal',
            type: 'text',
            name: 'login',
            required: true
          },
          )),
        React.createElement('label', { className: 'form__input-wrapper form__input-wrapper--modal' },
          React.createElement('span', { className: 'form__text form__text--modal' }, 'Пароль'),
          React.createElement('input', {
            className: 'form__input form__input--modal',
            type: 'password',
            name: 'password',
            required: true
          },
          )),
        React.createElement('button', { className: 'form__modal-submit', type: 'submit' }, 'Войти'),
      ),
      React.createElement('div', { className: 'modal__links', action: '#' },
        React.createElement('div', { className: 'modal__link-wrapper' },
          React.createElement('p', { className: 'modal__text modal__text--indent' }, 'Нет учетной записи?'),
          React.createElement('a', { className: 'modal__link modal__link--reg', href: '#' }, 'Регистрация')),
        React.createElement('div', { className: 'modal__link-wrapper modal__link-wrapper--indent' },
          React.createElement('p', { className: 'modal__text' }, 'Забыли пароль?'),
          React.createElement('a', { className: 'modal__link', href: '#' }, 'Восстановить пароль')),
      ),
    )
  )
};

const auth = React.createElement;
const authRoot = ReactDOM.createRoot(document.querySelector('.authorization-container'));
authRoot.render(auth(Auth));

$(document).ready(() => {
  $('.modal__close-auth-button').click((evt) => {
    evt.preventDefault();
    $('.modal-auth').fadeOut(300);
    $('.page').removeClass('page--modal-auth')
    window.onscroll = () => { };
  });
});

$(document).ready(() => {
  $('.form').submit((evt) => {
    evt.preventDefault();
  });
});


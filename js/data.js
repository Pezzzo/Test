'use strict';

const URL = 'https://api.opendota.com/api/teams';

const getData = async (URL) => {
  return await fetch(URL)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Could not find data');
      }
    })
    .catch((error) => {
      console.error(error);
    });
};

setInterval(() => getData(URL).then(data => {
  console.log(data);
 }), 60000);


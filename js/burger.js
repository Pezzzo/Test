'use strict';

$(document).ready(() => {
  $('.burger-menu__button').click((evt) => {
    evt.preventDefault();
    $('.burger-menu__button').toggleClass('burger-menu__button--opened');
    if ($('.burger').is(':hidden')) {
      $('.burger').fadeIn(300);
    }
    else {
      $('.burger').fadeOut(300);
    }
  });
});

const Menu = () => {
  return React.createElement('div', { className: 'burger' },
    React.createElement('div', { className: 'burger__nav' },
      React.createElement('form', { className: 'form form--burger', action: '#' },
        React.createElement('label', { className: 'form__input-wrapper' },
          React.createElement('input', { className: 'form__input form__input--burger', type: 'text', name: 'search', placeholder: 'Найти профиль' })),
        React.createElement('button', { className: 'form__submit' })),
      React.createElement('nav', { className: 'page-nav page-nav--burger' },
        React.createElement('a', { className: 'page-nav__link page-nav__link--burger page-nav__link--burger-mobile', href: 'index.html' }, 'Главная'),
        React.createElement('a', { className: 'page-nav__link page-nav__link--burger', href: 'matches.html' }, 'Список матчей'),
        React.createElement('a', { className: 'page-nav__link page-nav__link--burger', href: 'teams.html' }, 'Список команд'),
        React.createElement('a', { className: 'page-nav__link page-nav__link--burger-disabled', href: '#' }, 'Сравнение команд')),
    ),
    React.createElement('div', { className: 'auth auth--burger' },
      React.createElement('a', { className: 'auth__link auth__link--burger', href: '#' },
        React.createElement('span', { className: 'auth__text auth__text--burger' }, 'Вход'))),
  )
};

const burger = React.createElement;
const burgerRoot = ReactDOM.createRoot(document.querySelector('.burger-container'));
burgerRoot.render(burger(Menu));

